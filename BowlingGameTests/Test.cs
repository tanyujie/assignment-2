﻿using NUnit.Framework;
using System;
using BowlingGame;

namespace BowlingGameTests
{
    [TestFixture()]
    public class Test
    {

        Game game;

        [SetUp]

        public void SetUpGame()
        {
            game = new Game();
        }

        void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }

        [Test]
        public void RollGutterGame()
        {


            game.Roll(0);

            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void RollOnes()
        {


            for (int i = 0; i < 20; i++)
            {
                game.Roll(1); 
            }

            Assert.That(game.Score(), Is.EqualTo(20));

        }

        [Test]
        public void RollSpareFirstFrame()
        {


            game.Roll(9);
            game.Roll(1);
            RollMany(18, 1);

            Assert.That(game.Score(), Is.EqualTo(29));
        }

    }
}
